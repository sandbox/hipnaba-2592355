<?php
/**
 * @file
 *
 * A Drush wrapper for BowerPHP inspired by Composer
 * (https://www.drupal.org/project/composer).
 */

/**
 * Implements hook_drush_command().
 *
 * @return array
 *   Commands defined in this file.
 *
 * @see hook_drush_command()
 */
function tiara_bower_drush_command() {
  $items['tiara-bower'] = array(
    'aliases' => array('tb', 'bower'),
    'arguments' => array(
      'command' => 'Run "drush bower" for a list of available commands.',
    ),
    'allow-additional-options' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_NONE
  );
  return $items;
}

/**
 * Runs BowerPHP.
 */
function drush_tiara_bower() {
  // Enable autoloading.
  // @todo Check for other autoloaders that have BowerPHP.
  if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    include __DIR__ . '/vendor/autoload.php';
  }

  // Ensure we have BowerPHP installed.
  if (!class_exists('Bowerphp\Console\Application')) {
    // Ask if the user would like to install BowerPHP.
    if (!drush_confirm(dt('BowerPHP not detected. Do you want to download and install it?'))) {
      drush_print(dt('Download of BowerPHP was aborted. Please run `composer install` in !path.', array('!path' => __DIR__)));
      return TRUE;
    }

    // Check if Drush Composer is available.
    if (!drush_is_command('composer')) {
      // Ask the user would like to install Drush Composer.
      if (!drush_confirm(dt('Download and install the Drush Composer extension?'))) {
        drush_print(dt('Installation of the Drush Composer extension was aborted.'));
        return TRUE;
      }

      // Download the Drush extension.
      if (drush_invoke('dl', array('composer-8.x-1.x')) === FALSE) {
        return drush_set_error(dt('Failed installing the Drush Composer extension. Install it manually by following the instructions on the project page: http://drupal.org/project/composer'));
      }

      // We can't use _drush_find_commandfiles because we're not in Drupal.
      drush_preflight();
      drush_get_commands(TRUE);
      drush_cache_clear_drush();
    }

    // Assemble composer arguments
    $composer_arguments = array();
    $composer_arguments[] = sprintf('--working-dir=%s', __DIR__);
    $composer_arguments[] = 'install';

    // Composer needs a lot of memory.
    if (function_exists('ini_set')) {
      ini_set('memory_limit', '512M');
    }

    // Install BowerPHP
    if (drush_invoke('composer', $composer_arguments) === FALSE) {
      return drush_set_error(dt('Failed installing BowerPHP. Install it manually by running `composer install` in !path.', array('!path' => __DIR__)));
    }

    include __DIR__ . '/vendor/autoload.php';
  }

  // Set up the arguments.
  $argv = func_get_args();
  if (!empty($argv)) {
    array_unshift($argv, 'tiara-bower');
  }
  else {
    $argv = array_slice($_SERVER['argv'], 3);
  }
  $input = new \Symfony\Component\Console\Input\ArgvInput($argv);

  // Set up the BowerPHP application.
  $application = new \Bowerphp\Console\Application();
  $application->setAutoExit(FALSE);

  // Run BowerPHP.
  return $application->run($input);
}